import React from "react";
import { DateRangePicker } from "react-date-range";
import { subDays } from "date-fns";

export default class DatePicker extends React.Component {
  
  render() {
    return (
      <div
        id="period-date"
        className="card"
        style={{ paddingLeft: 0, marginTop: "10px" }}
      >
        <div
          style={{
            display: "flex",
            alignItems: "center",
            flexDirection: "row",
            fontWeight: 600,
            padding: "0 0 10px 20px",
            width: "100%",
          }}
        >
          <i id="calendar"></i>
          <p style={{ marginLeft: "10px" }}>Period</p>
          <i
            id="close"
            style={{ marginLeft: "auto" }}
            onClick={this.props.closeDatePicker}
          ></i>
        </div>
        <DateRangePicker
          ranges={[this.props.state.selection]}
          onChange={this.props.handleChange}
          minDate={subDays(new Date(), 180)}
          maxDate={subDays(new Date(), 1)}
          showSelectionPreview={false}
          months={2}
          rangeColors={["#37B04C"]}
          color={"#37B04C"}
          direction="horizontal"
          showPreview={false}
        />
        <button id="btn-apply" onClick={this.props.applyDatePicker}>
          Apply
        </button>
      </div>
    );
  }
}
