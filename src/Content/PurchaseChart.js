import React, { PureComponent } from "react";
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
} from "recharts";

const data = [
  {
    name: "Sun",
    Nett: 4000,
    Gross: 2400,
    APV: 2400,
    UPT: 100,
  },
  {
    name: "Mon",
    Nett: 4000,
    Gross: 2400,
    APV: 2400,
    UPT: 100,
  },
  {
    name: "Tue",
    Nett: 4000,
    Gross: 2400,
    APV: 2400,
    UPT: 100,
  },
  {
    name: "Wed",
    Nett: 4000,
    Gross: 2400,
    APV: 2400,
    UPT: 100,
  },
  {
    name: "Thu",
    Nett: 4000,
    Gross: 2400,
    APV: 2400,
    UPT: 100,
  },
  {
    name: "Fri",
    Nett: 4000,
    Gross: 2400,
    APV: 2400,
    UPT: 100,
  },
  {
    name: "Sat",
    Nett: 4000,
    Gross: 2400,
    APV: 2400,
    UPT: 100,
  },
];

export default class PurchaseChart extends PureComponent {
  render() {
    return (
      <BarChart
        width={500}
        height={300}
        data={data}
        margin={{
          top: 0,
          right: 0,
          left: 0,
          bottom: 0,
        }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="name" />
        <YAxis />
        <Tooltip />
        <Legend />
        <Bar dataKey="Nett" stackId="a" fill="#37B04C" />
        <Bar dataKey="Gross" stackId="a" fill="#289E45" />
        <Bar dataKey="APV" stackId="a" fill="#7AE28C" />
        <Bar dataKey="UPT" stackId="a" fill="#707070" />
      </BarChart>
    );
  }
}
