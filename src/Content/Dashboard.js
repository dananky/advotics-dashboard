import React from "react";
import { subDays } from "date-fns";
import moment from "moment";
import DatePicker from "./DatePicker.js";
import PurchaseChart from "./PurchaseChart";

export default class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showDatePicker: false,
      showMarketInsight: true,
      selection: {
        startDate: subDays(new Date(), 7),
        endDate: subDays(new Date(), 1),
        selection: "select",
      },
      startDate: subDays(new Date(), 7),
      endDate: subDays(new Date(), 1),
    };
  }

  handleChange = (ranges) => {
    console.log(ranges);
    this.setState(
      {
        selection: ranges.range1,
      },
      () => console.log(this.state.selection)
    );
  };

  applyDatePicker = () => {
    this.setState(
      {
        startDate: this.state.selection.startDate,
        endDate: this.state.selection.endDate,
      },
      () => {
        this.closeDatePicker();
      }
    );
  };

  showDatePicker = () => {
    this.setState({
      showDatePicker: !this.state.showDatePicker,
    });
  };

  closeDatePicker = () => {
    this.setState({
      showDatePicker: false,
    });
  };

  showHideMarketInsight = () => {
    this.setState({
      showMarketInsight: !this.state.showMarketInsight,
    });
  };

  render() {
    return (
      <div id="page-wrapper">
        <div id="container">
          <div id="page-header">
            <div id="page-title">Dashboard</div>
            <div id="filter-period">
              <i id="calendar"></i>
              <p>Period</p>
              <div id="period">
                <p>
                  {moment(this.state.startDate).format("DD MMMM YYYY")}
                  {" - "}
                  {moment(this.state.endDate).format("DD MMMM YYYY")}
                </p>
                <i
                  id={this.state.showDatePicker ? "arrow-up" : "arrow-down"}
                  onClick={this.showDatePicker}
                ></i>
              </div>
            </div>
          </div>
          <div id="date-picker">
            {this.state.showDatePicker && (
              <DatePicker
                state={this.state}
                closeDatePicker={this.closeDatePicker}
                applyDatePicker={this.applyDatePicker}
                handleChange={this.handleChange}
              />
            )}
          </div>
          <div id="content-header">
            <div id="content-title">MARKET INSIGHT</div>
            <div id="help-side">
              <i id="help"></i>
              <p>Click Here for Help</p>
              <i
                id={
                  this.state.showMarketInsight
                    ? "arrow-white-up"
                    : "arrow-white-down"
                }
                onClick={this.showHideMarketInsight}
              ></i>
            </div>
          </div>
          {this.state.showMarketInsight && (
            <>
              <div className="card" id="sales-turnover">
                <div id="row-1">
                  <p>Sales Turnover</p>
                  <i id="more"></i>
                </div>
                <div id="row-2">
                  <div>
                    <span id="sales-value">Rp 3,600,000</span>
                    <p>
                      <i id="arrows-down"></i>{" "}
                      <span style={{ color: "#FF4141", fontWeight: "bold" }}>
                        13.8%
                      </span>{" "}
                      last period in products sold
                    </p>
                  </div>
                  <i id="sales-turnover-icon"></i>
                </div>
              </div>
              <div id="sales-wrap">
                <div className="card" id="purchase-chart">
                  <div id="row-1">
                    <p>AVERAGE PURCHASE VALUE</p>
                    <i id="more"></i>
                  </div>
                  <div style={{ marginTop: "50px" }}>
                    <PurchaseChart />
                  </div>
                </div>
                <div className="card" id="best-selling">
                  <div id="row-1">
                    <p>BEST SELLING SKU</p>
                    <i id="more"></i>
                  </div>
                  <div id="row-3">
                    <div id="top-item">
                      <i id="image-top"></i>
                      <div id="item-info">
                        <p>[Nama Product]</p>
                        <div id="item-count">
                          <p>Rp [XXX]</p>
                          <p>[jml terjual]</p>
                        </div>
                      </div>
                    </div>
                    <div id="list-item">
                      <i id="image-product"></i>
                      <div id="item-info">
                        <p>[Nama Product]</p>
                        <div id="item-count">
                          <p>Rp [XXX]</p>
                          <p>[jml terjual]</p>
                        </div>
                      </div>
                    </div>
                    <div id="list-item">
                      <i id="image-product"></i>
                      <div id="item-info">
                        <p>[Nama Product]</p>
                        <div id="item-count">
                          <p>Rp [XXX]</p>
                          <p>[jml terjual]</p>
                        </div>
                      </div>
                    </div>
                    <div id="list-item">
                      <i id="image-product"></i>
                      <div id="item-info">
                        <p>[Nama Product]</p>
                        <div id="item-count">
                          <p>Rp [XXX]</p>
                          <p>[jml terjual]</p>
                        </div>
                      </div>
                    </div>
                    <div id="list-item">
                      <i id="image-product"></i>
                      <div id="item-info">
                        <p>[Nama Product]</p>
                        <div id="item-count">
                          <p>Rp [XXX]</p>
                          <p>[jml terjual]</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="card" id="top-competitor">
                  <div id="row-1">
                    <p>TOP KOMPETITOR</p>
                    <i id="more"></i>
                  </div>
                  <div id="row-3">
                    <div id="top-item">
                      <i id="image-top"></i>
                      <div id="item-info">
                        <p>[Nama Product]</p>
                        <div id="item-count">
                          <p>Rp [XXX]</p>
                          <p>[jml terjual]</p>
                        </div>
                      </div>
                    </div>
                    <div id="list-item">
                      <i id="image-product"></i>
                      <div id="item-info">
                        <p>[Nama Product]</p>
                        <div id="item-count">
                          <p>Rp [XXX]</p>
                          <p>[jml terjual]</p>
                        </div>
                      </div>
                    </div>
                    <div id="list-item">
                      <i id="image-product"></i>
                      <div id="item-info">
                        <p>[Nama Product]</p>
                        <div id="item-count">
                          <p>Rp [XXX]</p>
                          <p>[jml terjual]</p>
                        </div>
                      </div>
                    </div>
                    <div id="list-item">
                      <i id="image-product"></i>
                      <div id="item-info">
                        <p>[Nama Product]</p>
                        <div id="item-count">
                          <p>Rp [XXX]</p>
                          <p>[jml terjual]</p>
                        </div>
                      </div>
                    </div>
                    <div id="list-item">
                      <i id="image-product"></i>
                      <div id="item-info">
                        <p>[Nama Product]</p>
                        <div id="item-count">
                          <p>Rp [XXX]</p>
                          <p>[jml terjual]</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </>
          )}
        </div>
      </div>
    );
  }
}
