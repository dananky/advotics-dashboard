import React from "react";
import ReactDOM from "react-dom";
import { Route, BrowserRouter as Router, Switch } from "react-router-dom";
import Navbar from "./Nav/Navbar";
import Sidebar from "./Nav/Sidebar";
import Dashboard from "./Content/Dashboard";
import * as serviceWorker from "./serviceWorker";
import "./Assets/Css/custom-style.css";
import "react-date-range/dist/styles.css";
import "react-date-range/dist/theme/default.css";

class IndexRouter extends React.Component {
  render() {
    return (
      <Router>
        <Navbar />
        <Sidebar />
        <Switch>
          <Route exact path="/" component={Dashboard} />
        </Switch>
      </Router>
    );
  }
}

ReactDOM.render(<IndexRouter />, document.getElementById("root"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
