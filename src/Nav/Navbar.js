import React from "react";
import logo from "../Assets/Icons/logo.jpg";
import logout from "../Assets/Icons/logout@2x.png";
import profile from "../Assets/Icons/profile.svg";

export default class Navbar extends React.Component {
  initial = () => {
    this.props.history.push("/");
  };

  render() {
    return (
      <header id="top-nav">
        <img id="logo" src={logo} alt="logo" />
        <div id="powered">
          powered by <i id="mini-logo"></i>
        </div>
        <nav id="nav-item">
          <a href={this.initial}>
            <div>
              Username <span>Company Name</span>
            </div>
          </a>
          <a href={this.initial}>
            <img id="profile" src={profile} alt="profile" />
          </a>
          <a href={this.initial}>
            <img id="logout" src={logout} alt="logout" />
          </a>
        </nav>
      </header>
    );
  }
}
