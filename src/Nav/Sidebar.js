import React from "react";
import dash from "../Assets/Icons/dash.svg";

export default class Sidebar extends React.Component {
  initial = () => {
    this.props.history.push("/");
  };

  render() {
    return (
      <aside id="side-nav">
        <div id="toggle-menu">
          <div id="menu">
            <div id="line-1"></div>
            <div id="line-2"></div>
            <div id="line-3"></div>
          </div>
        </div>
        <a id="active" href={this.initial}>
          <span>
            <img src={dash} alt="dashboard" />
          </span>
        </a>
      </aside>
    );
  }
}
